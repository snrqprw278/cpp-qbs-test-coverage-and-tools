CppApplication {
    type: base.concat("autotest")
    files: [
        "mytest.cpp",
    ]

    cpp.driverFlags: "-pthread"

    Depends { name: "gtest" }
    Depends { name: "lib" }
}
