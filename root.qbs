import qbs.FileInfo
import qbs.Probes

Project {
    references: [
        "src/src.qbs",
        "tests/tests.qbs",

        FileInfo.joinPaths(conan.generatedFilesPath, "conanbuildinfo.qbs")
    ]

    Product {
        name: "misc"

        files: [
            "conanfile.py",
            ".gitlab-ci.yml",
        ]
    }

    Probes.ConanfileProbe {
        id: conan
        conanfilePath: FileInfo.joinPaths(sourceDirectory, "conanfile.py")
        additionalArguments: {
            var args = [];
            return args;
        }
    }
}
