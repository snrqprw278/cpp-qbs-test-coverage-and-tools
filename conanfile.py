from conans import ConanFile, tools


class CppQbsTestCoverageToolsConan(ConanFile):
    name = "Cpp Qbs Test Coverage Tools"
    version = "0.0.1"
    license = "MIT"
    author = "Kai Dohmen psykai1993@googlemail.com"
    settings = "os", "compiler"
    exports_sources = [
        "*.cpp",
        "*.h",
        "*.qbs",
    ]
    no_copy_source = True
    build_requires = [
        "gtest/1.10.0",
    ]
    generators = [
        "json",
        "qbs",
    ]
