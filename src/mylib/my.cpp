#include "my.h"

namespace my {

Foobar::Foobar(UseInterface &use) noexcept : m_use{use} {}

int Foobar::doSomething(int mult) noexcept
{
    if (mult == 0) {
        return 0;
    }
    if (mult < 0) {
        mult = -mult;
    }
    return m_use.useMe() * mult;
}

} // namespace my
