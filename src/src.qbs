Project {
    StaticLibrary {
        name: "lib"

        files: [
            "mylib/my.cpp",
            "mylib/my.h",
        ]

        cpp.cxxLanguageVersion: "c++17"

        Depends { name: "cpp" }

        Export {
            cpp.includePaths: product.sourceDirectory            
            cpp.cxxLanguageVersion: "c++17"

            Depends { name: "cpp" }
        }
    }
}
